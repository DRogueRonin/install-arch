#!/bin/sh

# wiki
# https://wiki.archlinux.org/title/MacBookPro10,x

# #
# Pre-Script Setup
# #

# loadkeys de-latin1

# https://wiki.archlinux.org/title/Fdisk
# https://wiki.archlinux.org/title/Swap
# fdisk /dev/$DISK
# new partition
# set bootable
# new partition swap
# mkfs.ext4 /dev/$PART
# mkswap /dev/$SWAP

# iwctl --passphrase $PHRASE station wlan0 connect $SSID


PART=
SWAP=false
USER=dweipert

OPTS=$(getopt -o '' -l 'part:,swap::,user::' -- "$@")
eval set -- "$OPTS"

while true; do
  case "$1" in
    --part) PART="$2"; shift 2 ;;
    --swap) SWAP="$2"; shift 2 ;;
    --user) USER="$2"; shift 2 ;;
    --) shift; break ;;
    *) echo "ERROR"; break ;;
  esac
done


mount /dev/$PART /mnt
$SWAP && swapon /dev/$SWAP

timedatectl set-ntp true

reflector --country Germany --protocol https --latest 15 --save /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel linux linux-headers linux-firmware \
  iwd dhcpcd \
  broadcom-wl-dkms # for MacBookPro10,2

genfstab -U /mnt >> /mnt/etc/fstab


# #
# Pre chroot
# #

sed -i 's/#de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /mnt/etc/locale.gen
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /mnt/etc/locale.gen
sed -i 's/#ja_JP.UTF-8 UTF-8/ja_JP.UTF-8 UTF-8/' /mnt/etc/locale.gen


# #
# chroot
# #

arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "LC_TIME=de_DE.UTF-8" >> /etc/locale.conf

echo "KEYMAP=de-latin1" > /etc/vconsole.conf

echo "archbook" > /etc/hostname

echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.0.1 archbook.local archbook" >> /etc/hosts

# create user
useradd -G wheel -m $USER
passwd $USER

sed -i 's/# %wheel/%wheel/' /etc/sudoers

systemctl enable iwd
systemctl enable dhcpcd

echo "DONE"
echo "SETUP BOOTLOADER YOURSELF"
# https://wiki.archlinux.org/title/REFInd
# https://wiki.archlinux.org/title/GRUB
exit


# #
# Post Install
# #

mkdir -p $HOME/Applications
mkdir -p $HOME/Documents
mkdir -p $HOME/Downloads
mkdir -p $HOME/Images

# AUR
REPO="[options]
CacheDir = /var/cache/pacman/pkg
CacheDir = /var/cache/pacman/aurutils
CleanMethod = KeepCurrent

[aurutils]
SigLevel = Optional TrustAll
Server = file:///var/cache/pacman/aurutils"
sudo echo $REPO >> /etc/pacman.d/aurutils
sudo echo "Include = /etc/pacman.d/aurutils" >> /etc/pacman.conf

sudo install -d /var/cache/pacman/aurutils -o $USER
repo-add /var/cache/pacman/aurutils/aurutils.db.tar

sudo pacman -Syu git neovim
export AUR_PAGER=nvim

git clone https://aur.archlinux.org/aurutils.git $HOME/.local/share/aurutils/git
cd $HOME/.local/share/aurutils/git
makepkg -irs

# dotfiles
sudo pacman -Sy yadm
yadm clone https://gitlab.com/drogueronin/dotfiles

# CLI
sudo pacman -Sy \
  openssh zsh man-db \
  eza ripgrep fd starship \
  rclone fuse2 imagemagick \
  btop trash-cli pass git-bug \
  noto-fonts noto-fonts-cjk noto-fonts-emoji

yadm bootstrap

# Desktop
sudo pacman -Sy \
  sway swaybg alacritty waybar \
  libnotify mako \
  xorg-xwayland wofi \
  gammastep \
  alsa-utils brightnessctl \
	cups cups-pdf sane \
  imv grim slurp mpv \
  keepassxc wl-clipboard \
  qutebrowser \
	khal vdirsyncer

sudo systemctl enable cups.service --now
systemctl --user enable vdirsyncer.timer --now

# AUR installs
aur sync -r nerd-fonts-hack
aur sync -r autojump
aur sync -r sway-services-git
aur sync -r nnn-nerd

sudo pacman -Sy \
  nerd-fonts-hack \
  autojump \
  sway-services-git \
  nnn-nerd

# Japanese input
aur sync -ru fcitx5-mozc-ut
sudo pacman -Sy \
  fcitx5-mozc-ut \
  fcitx5-qt fcitx5-gtk \
  fcitx5-configtool

# printer
aur sync -r epson-inkjet-printer-escpr
sudo pacman -Sy epson-inkjet-printer-escpr

aur sync -r brother-dcpl3510cdw-drv-bin
aur sync -r brscan4
sudo pacman -Sy \
  brother-dcpl3510cdw-drv-bin \
  brscan4

sudo ln -s /opt/brother/Printers/dcpl3510cdw/cupswrapper/brother_lpdwrapper_dcpl3510cdw /usr/lib/cups/filter/
# https://support.brother.com/g/b/downloadhowto.aspx?c=de&lang=de&prod=dcpl3510cdw_eu_as&os=128&dlid=dlf105201_000&flang=4&type3=565
sudo brsaneconfig4 -a name="Brother DCP-L3510CDW" model="DCP-L3510CDW" ip="IP"


# nix
sudo pacman -Sy \
    nix

sed -i 's/# sandbox/sandbox/' /etc/nix/nix.conf

sudo systemctl enable nix-daemon --now

sudo usermod -aG nix-users $USER

nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update
nix-env -u

## nix - debug help with "nix repl ."
echo "import <nixpkgs> {}" > ~/default.nix

# in case of "warning: Nix search path entry '/nix/var/nix/profiles/per-user/$USER/channels' does not exist, ignoring"
sudo mkdir /nix/var/nix/profiles/per-user/$USER
sudo ln -s $HOME/.nix-defexpr/channels /nix/var/nix/profiles/per-user/$USER/channels


# #
# Problem solving
# #

# in case of backlight problems
# acpi_osi=Linux

# SD Card Reader fix
# https://askubuntu.com/questions/695944/sd-card-doesnt-work-well-under-ubuntu-but-does-well-under-windows/704028#704028
# https://cialu.net/how-to-solve-broadcom-sd-card-reader-issue-with-linux/
# https://bbs.archlinux.org/viewtopic.php?id=244223
sudo echo "options sdhci debug_quirks=0x40" >> /etc/modprobe.d/sdhci-pci.conf
sudo echo "options sdhci debug_quirks2=4" >> /etc/modprobe.d/sdhci-pci.conf
sudo modprobe -r sdhci-pci sdhci
sudo modprobe sdhci-pci
